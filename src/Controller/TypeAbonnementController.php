<?php

namespace App\Controller;

use App\Entity\Contrat;
use App\Entity\TypeAbonnement;
use App\Form\TypeAbonnementType;
use App\Repository\TypeAbonnementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type_abonnement")
 */
class TypeAbonnementController extends AbstractController
{
    /**
     * @Route("/", name="type_abonnement_index", methods={"GET"})
     */
    public function index(TypeAbonnementRepository $typeAbonnementRepository): Response
    {
        $contratRepository = $this->getDoctrine()->getRepository(Contrat::class);
        $abonnements = $typeAbonnementRepository->findAll();

        $tab_abonnement = [];
        $i = 0;
        foreach ($abonnements as $abonnement) {
            $nb_contrats = $contratRepository->findBy(['typeAbonnement' => $abonnement->getId()]);

            $tab_abonnement[$i]['id'] = $abonnement->getId();
            $tab_abonnement[$i]['nom'] = $abonnement->getNom();
            $tab_abonnement[$i]['nombre'] = count($nb_contrats);

            $i++;
        }

        return $this->render('admin/type_abonnement/index.html.twig', [
            'type_abonnements' => $abonnements,
            'nb_abonnement' => $tab_abonnement
        ]);
    }

    /**
     * @Route("/new", name="type_abonnement_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeAbonnement = new TypeAbonnement();
        $form = $this->createForm(TypeAbonnementType::class, $typeAbonnement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeAbonnement);
            $entityManager->flush();

            return $this->redirectToRoute('type_abonnement_index');
        }

        return $this->render('admin/type_abonnement/new.html.twig', [
            'type_abonnement' => $typeAbonnement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_abonnement_show", methods={"GET"})
     */
    public function show(TypeAbonnement $typeAbonnement): Response
    {
        return $this->render('admin/type_abonnement/show.html.twig', [
            'type_abonnement' => $typeAbonnement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_abonnement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeAbonnement $typeAbonnement): Response
    {
        $form = $this->createForm(TypeAbonnementType::class, $typeAbonnement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_abonnement_index');
        }

        return $this->render('admin/type_abonnement/edit.html.twig', [
            'type_abonnement' => $typeAbonnement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_abonnement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeAbonnement $typeAbonnement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeAbonnement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeAbonnement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_abonnement_index');
    }
}
