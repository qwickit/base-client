<?php

namespace App\Controller;

use App\Entity\Contrat;
use App\Entity\TypeSite;
use App\Form\TypeSiteType;
use App\Repository\TypeSiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type_site")
 */
class TypeSiteController extends AbstractController
{
    /**
     * @Route("/", name="type_site_index", methods={"GET"})
     */
    public function index(TypeSiteRepository $typeSiteRepository): Response
    {
        $contratRepository = $this->getDoctrine()->getRepository(Contrat::class);
        $sites = $typeSiteRepository->findAll();

        $tab_site = [];
        $i = 0;
        foreach ($sites as $site) {
            $nb_contrats = $contratRepository->findBy(['typeSite' => $site->getId()]);

            $tab_site[$i]['id'] = $site->getId();
            $tab_site[$i]['nom'] = $site->getNom();
            $tab_site[$i]['nombre'] = count($nb_contrats);

            $i++;
        }

        return $this->render('admin/type_site/index.html.twig', [
            'type_sites' => $sites,
            'nb_sites' => $tab_site
        ]);
    }

    /**
     * @Route("/new", name="type_site_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeSite = new TypeSite();
        $form = $this->createForm(TypeSiteType::class, $typeSite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeSite);
            $entityManager->flush();

            return $this->redirectToRoute('type_site_index');
        }

        return $this->render('admin/type_site/new.html.twig', [
            'type_site' => $typeSite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_site_show", methods={"GET"})
     */
    public function show(TypeSite $typeSite): Response
    {
        return $this->render('admin/type_site/show.html.twig', [
            'type_site' => $typeSite,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_site_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeSite $typeSite): Response
    {
        $form = $this->createForm(TypeSiteType::class, $typeSite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_site_index');
        }

        return $this->render('admin/type_site/edit.html.twig', [
            'type_site' => $typeSite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_site_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeSite $typeSite): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeSite->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeSite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_site_index');
    }
}
