<?php

namespace App\Controller;

use App\Entity\Contrat;
use App\Entity\Client;
use App\Entity\TypeSite;
use App\Entity\TypeAbonnement;
use App\Form\ContratType;
use App\Repository\ContratRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("admin/contrat")
 */
class ContratController extends AbstractController
{
    /**
     * @Route("/", name="contrat_index", methods={"GET"})
     */
    public function index(): Response
    {
        // Récupère les contrats
        $contratRepository = $this->getDoctrine()->getRepository(Contrat::class);
        $contrats = $contratRepository->findAll();

        $clientRepository = $this->getDoctrine()->getRepository(Client::class);
        $typeAboRepository = $this->getDoctrine()->getRepository(TypeAbonnement::class);

        $tab_contrats = [];
        $i = 0;

        foreach ($contrats as $contrat) {
            $client = $clientRepository->findOneById($contrat->getClient());
            $typeAbo = $typeAboRepository->findOneById($contrat->getTypeAbonnement());

            $tab_contrats[$i]['id'] = $contrat->getId();
            $tab_contrats[$i]['client'] = $client->getEntreprise();
            $tab_contrats[$i]['type_abonnement'] = $typeAbo->getNom();
            $tab_contrats[$i]['prix_abonnement'] = $contrat->getPrixAbonnement();
            $tab_contrats[$i]['date_expiration'] = $contrat->getDateExpiration();

            $i++;
        }

        return $this->render('admin/contrat/index.html.twig', [
            'contrats' => $tab_contrats,
        ]);
    }

    /**
     * @Route("/new", name="contrat_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $contrat = new Contrat();
        $form = $this->createForm(ContratType::class, $contrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contrat);
            $entityManager->flush();

            return $this->redirectToRoute('contrat_index');
        }

        return $this->render('admin/contrat/new.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contrat_show", methods={"GET"})
     */
    public function show(Contrat $contrat): Response
    {

        // Récupère le client
        $clientRepository = $this->getDoctrine()->getRepository(Client::class);
        $client = $clientRepository->findOneById($contrat->getClient());

        // Récupère le type de site
        $typesiteRepository = $this->getDoctrine()->getRepository(TypeSite::class);
        $typeSite = $typesiteRepository->findOneById($contrat->getTypeSite());

        // Récupère le type d'abonnement
        $typeAboRepository = $this->getDoctrine()->getRepository(TypeAbonnement::class);
        $typeAbo = $typeAboRepository->findOneById($contrat->getTypeAbonnement());

        return $this->render('admin/contrat/show.html.twig', [
            'contrat' => $contrat,
            'client' => $client,
            'typeSite' => $typeSite,
            'abonnement' => $typeAbo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="contrat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contrat $contrat): Response
    {
        $form = $this->createForm(ContratType::class, $contrat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contrat_index');
        }

        return $this->render('admin/contrat/edit.html.twig', [
            'contrat' => $contrat,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contrat_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contrat $contrat): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contrat->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contrat);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contrat_index');
    }
}
