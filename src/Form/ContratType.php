<?php

namespace App\Form;

use App\Entity\Contrat;
use App\Entity\Client;
use App\Entity\TypeSite;
use App\Entity\TypeAbonnement;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContratType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'entreprise',
                'multiple' => false,
            ])
            ->add('typeSite', EntityType::class, [
                'class' => TypeSite::class,
                'choice_label' => 'nom',
                'multiple' => false,
            ])
            ->add('prix')
            ->add('prix_abonnement')
            ->add('typeAbonnement', EntityType::class, [
                'class' => TypeAbonnement::class,
                'choice_label' => 'nom',
                'multiple' => false,
            ])
            ->add('duree_engagement')
            ->add('date_creation')
            ->add('date_expiration')
            ->add('commentaires')
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contrat::class,
        ]);
    }
}
